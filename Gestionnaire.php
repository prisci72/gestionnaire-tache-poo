<?php

class Gestionnaire {
    public $taches = [];

    function ajouterTache($tache){
        $this->taches[] = $tache;
    }

    function afficherListe(){
        foreach($this->taches as $tache){
            echo $tache->nom . " (" . $tache->id . ")\n";
        }
    }
    
}