<?php

require "Tache.php";
require "Gestionnaire.php";

$gestionnaire = new Gestionnaire();
$idTache = 1;

do{
    echo "Que souhaitez-vous faire ? (aide, tache, liste, supprimer, quitter )\n>";
    $rep = trim(fgets(STDIN));

    switch($rep){
        case 'aide' :
        case 'a' :
        echo "Liste des commandes possibles : \n
- aide/a afficher les commandes possibles \n
- tache/t créer une nouvelle tâche \n
- liste/l afficher la liste des tâches en cours \n
- supprimer/s supprime une tache existante \n
- quitter/q arrêter l'éxécution du script \n";
        break;

        case 'tache':
        case 't':
                    echo "Veuillez entrer le nom de la tache :\n> ";
                    $nomTache = trim(fgets(STDIN));
                    $gestionnaire->ajouterTache(new Tache($nomTache,$idTache));
                    $idTache++;
        break;

        case 'liste';
        case 'l';
                    echo $gestionnaire->afficherListe() . "\n";
        break;
    }

} while ($rep != 'q' && $rep!='quitter'); 